//
//  MessageVM.swift
//  SR-Socket
//
//  Created by charoenmit on 13/6/2561 BE.
//  Copyright © 2561 charoenmit. All rights reserved.
//

import Foundation
import MessageKit

internal struct MessageVM: MessageType {
    
    var data: MessageData
    var messageId: String
    var sender: Sender
    var sentDate: Date
    
    private init(data: MessageData, sender: Sender, messageId: String, date: Date) {
        self.data = data
        self.sender = sender
        self.messageId = messageId
        self.sentDate = date
    }
    
    init(attributedText: NSAttributedString, sender: Sender, messageId: String, date: Date) {
        self.init(data: .attributedText(attributedText), sender: sender, messageId: messageId, date: date)
    }
    
}
