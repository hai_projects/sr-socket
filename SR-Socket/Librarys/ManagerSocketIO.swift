//
//  managerSocketIO.swift
//  SR-Socket
//
//  Created by charoenmit on 11/6/2561 BE.
//  Copyright © 2561 charoenmit. All rights reserved.
//

import UIKit
import SocketIO

class SocketIOManager: NSObject {
    
    static let sharedInstance = SocketIOManager()
    let manager = SocketManager(socketURL: URL(string: "http://13.229.205.31:8088")!, config: [.log(true), .compress])
   
    override init() {
        super.init()
    }
    
    func connectToServerWithRoom(withName name: String, withRoom room: String) {
        
        var roomName : [String:String] = [:]
        roomName["room"] = room
        manager.defaultSocket.emit("subscribe", roomName)
    }
    
    func getChatMessage(_ completionHandler: @escaping (_ messageInfo: [[String: AnyObject]]) -> Void) {
        
        manager.defaultSocket.on("syncMessage") { (dataArray, socketAck) -> Void in

            let theDictionary = dataArray[0]
            print("getChatMessage",theDictionary)
            completionHandler(theDictionary as! [[String : AnyObject]])
        }
    }
    
    func receiveChatMessage(_ completionHandler: @escaping (_ messageInfo: [String: AnyObject]) -> Void) {
        
        manager.defaultSocket.on("reciveMessage") { (dataArray, socketAck) -> Void in
            
            let theDictionary = dataArray[0]
            print("receiveChatMessage",theDictionary)
            completionHandler(theDictionary as! [String : AnyObject])
        }
    }
    
    func sendMessage(withText text: String, withName name: String, withId id: String, withRoom room: String) {
        
        var message : [String:String] = [:]
        message["room"] = room
        message["message"] = text
        message["name"] = name
        message["senderId"] = id
        manager.defaultSocket.emit("sendMessage", message)
    }
    
    func establishConnection() {
        manager.defaultSocket.connect()
    }
    
    func closeConnection() {
        manager.defaultSocket.disconnect()
    }
}
