//
//  ChatsVC.swift
//  SR-Socket
//
//  Created by charoenmit on 12/6/2561 BE.
//  Copyright © 2561 charoenmit. All rights reserved.
//

import UIKit

class ChatsVC: UITableViewController {
    
    let nameRoom = ["ChatA", "ChatB", "ChatC", "ChatD", "ChatE", "ChatF"]
    var name : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "SR-ChatDemo"
        askForName()
    }
    
    func askForName() {
        let alertController = UIAlertController(title: "Welcome", message: "Input name", preferredStyle: .alert)
        
        alertController.addTextField(configurationHandler: nil)
        
        let OKAction = UIAlertAction(title: "Sign in", style: .default) {(action) -> Void in
            let textfield = alertController.textFields![0]
            if textfield.text?.count == 0 {
                self.askForName()
            }
            else {
                guard let name = textfield.text else {return}
                self.name = name
            }
        }
        
        alertController.addAction(OKAction)
        present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func openUserListVC(_ sender: Any) {
        self.performSegue(withIdentifier: "UserListVCSegue", sender: nil)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nameRoom.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatCell", for: indexPath) as! ChatCell
        cell.nameLabel.text = nameRoom[indexPath.row]
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "ChatVCSegue", sender: indexPath)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            
            if segue.identifier == "UserListVCSegue" {
                let nav = segue.destination as! UINavigationController
                let controller = nav.topViewController as! UserListVC
                controller.name = self.name
            }
            else if segue.identifier == "ChatVCSegue" {
                let controller = segue.destination as! ChatVC
                controller.name = self.name
                let row = (sender as! NSIndexPath).row
                controller.room = nameRoom[row]
                controller.roomName = [nameRoom[row]]
            }
    }
    
}
