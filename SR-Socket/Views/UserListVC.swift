//
//  UserListVC.swift
//  SR-Socket
//
//  Created by charoenmit on 12/6/2561 BE.
//  Copyright © 2561 charoenmit. All rights reserved.
//

import UIKit

class UserListVC: UITableViewController {
    
    @IBOutlet weak var okButton: UIBarButtonItem!
    
    let users = ["UserA", "UserB", "UserC", "UserD", "UserE", "UserF"]
    var selectedUser: [String] = []
    var selectedRows = NSMutableIndexSet()
    var name: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Select users"
        okButton.isEnabled = false
    }
    
    @IBAction func popToChatsVC(_ sender: Any) {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func openChatVC(_ sender: Any) {
        self.performSegue(withIdentifier: "ChatVCSegue", sender: nil)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChatCell", for: indexPath) as! ChatCell
        cell.nameLabel.text = users[indexPath.row]
        
        var accessory = UITableViewCellAccessoryType.none
        if selectedRows.contains(indexPath.row) {
            accessory = .checkmark
        }
        
        cell.accessoryType = accessory
        
        
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.selectedRows.contains(indexPath.row) ? self.selectedRows.remove(indexPath.row) : self.selectedRows.add(indexPath.row)
        
        if self.selectedRows.contains(indexPath.row) {
            selectedUser.append(users[indexPath.row])
        }
        else {
            selectedUser = selectedUser.filter{$0 != users[indexPath.row]}
        }
        
        let rows = [IndexPath(row: 0, section: 0), indexPath]
    
        tableView.reloadRows(at: rows, with: .none)
        
        if selectedUser.isEmpty {
            okButton.isEnabled = false
        }
        else {
            okButton.isEnabled = true
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "ChatVCSegue" {
            let vc = segue.destination as! ChatVC
            vc.roomName = selectedUser
            vc.name = name
        }
    }
    
}
